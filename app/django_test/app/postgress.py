import psycopg2
import revdebug
import random
import string
from datetime import datetime

# print("REVDEBUG PYTHON CILENT VERSION: " +revdebug.__version__)
# print("REVDEBUG MODE: " +str(revdebug.config.mode))
class postgress_test:
    def __init__(self):
        self.HOST="40.89.147.34"
        self.DBNAME="postgres"
        self.USER="postgres"
        self.PASSWORD="docker"


    def execute(self,query):
        conn = psycopg2.connect( host=self.HOST, dbname=self.DBNAME, user=self.USER, password=self.PASSWORD)
        cur = conn.cursor()
        cur.execute(query)
        result = cur.fetchall()
        cur.close()
        conn.close()
        return result


    def execute_save(self, query):
        conn = psycopg2.connect( host=self.HOST, dbname=self.DBNAME, user=self.USER, password=self.PASSWORD)
        cur = conn.cursor()
        cur.execute(query)
        conn.commit()
        cur.close()
        conn.close()

    def chain(self):
        try:
            self.execute("SELECTerror1 * FROM mytabel")
        finally:
            try:
                self.execute("SELECTerror * FROM mytabel")
            finally:
                try:
                    self.execute("SELECTerror * FROM mytabel")
                finally:
                    try:
                        self.execute("SELECTerror * FROM mytabel")
                    finally:
                        self.execute("SELECTerror * FROM mytabel")
                        try:
                            self.execute("SELECTerror * FROM mytabel")
                        finally:
                            try:
                                self.execute("SELECTerror * FROM mytabel")
                            finally:
                                try:
                                    self.execute("SELECTerror * FROM mytabel")
                                finally:
                                    try:
                                        self.execute("SELECTerror * FROM mytabel")
                                    finally:
                                        self.execute("SELECT * FROM mytabel")



    def callproc(self):
        self.execute_save("DROP FUNCTION get_parts_by_vendor")
        func_str="CREATE OR REPLACE FUNCTION get_parts_by_vendor(id2 integer)   RETURNS TABLE(part_id INTEGER, part_name VARCHAR) AS $$ BEGIN  RETURN QUERY   SELECT mytabel.id, mytabel.name  FROM mytabel  INNER JOIN vendor_parts on vendor_parts.part_id = mytabel.id  WHERE  vendor_parts.part_id = id2;  END; $$  LANGUAGE plpgsql;"
        self.execute_save(func_str)
        try:
            conn = psycopg2.connect( host=self.HOST, dbname=self.DBNAME, user=self.USER, password=self.PASSWORD)
            cur = conn.cursor()
            cur.callproc('get_parts_by_vendor', ("1",))
            row = cur.fetchone()
            while row is not None:
                print(row)
                row = cur.fetchone()
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()

    def create_table(self):
        func_str="CREATE TABLE mytable ( id serial PRIMARY KEY, username VARCHAR ( 50 ) NOT NULL );"
        self.execute_save(func_str)
        
    def drop_table(self):
        func_str="DROP TABLE mytable"
        self.execute_save(func_str)

    def insert(self):
         # Execute a query
        s=''.join(random.choices(string.ascii_uppercase + string.digits, k=40))
        print(s)
        self.execute_save("INSERT INTO mytabel (id, name) VALUES ("+str(random.randint(0, 9999))+", '"+s+"');")

    def select(self,sql):
            self.execute(sql)

    
    def start(self):
        try:        
            self.callproc()
            self.create_table()
            self.insert()
            self.select("SELECT * FROM mytabel")
            self.select("SELECTsd * FROM mytabel")
        finally:
            self.drop_table()



