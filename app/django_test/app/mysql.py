import pymysql.cursors


class mysql_test:
    def connection_mysql(self):
        # Connect to the database
        connection = pymysql.connect(host='40.89.147.34',
                                    user='anubarak',
                                    password='anubarak',
                                    database='anubarak',
                                    cursorclass=pymysql.cursors.DictCursor)
        return connection

    
    def create_tabel(self):
        connection = self.connection_mysql()
        with connection:
            with connection.cursor() as cursor:
                sql = "CREATE TABLE `users` ( `id` int(11) NOT NULL AUTO_INCREMENT, `email` varchar(255) COLLATE utf8_bin NOT NULL, `password` varchar(255) COLLATE utf8_bin NOT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=1 ;"
                cursor.execute(sql)

    def insert(self):
        connection = self.connection_mysql()
        with connection:
            with connection.cursor() as cursor:
                sql = "INSERT INTO `users` (`email`, `password`) VALUES (%s, %s)"
                cursor.execute(sql, ('webmaster@python.org', 'very-secret'))
                connection.commit()

    def select(self, sql):
        connection = self.connection_mysql()
        with connection:
            with connection.cursor() as cursor:
                cursor.execute(sql, ('webmaster@python.org',))
                result = cursor.fetchone()
                print(result)

    def drop(self):
        connection = self.connection_mysql()
        with connection:
            with connection.cursor() as cursor:
                sql = "DROP TABLE users"
                cursor.execute(sql)
    def start():
        try:
            mysql=mysql_test()

            mysql.create_tabel()

            mysql.insert()

            mysql.select("SELECT `id`, `password` FROM `users` WHERE `email`=%s")

            mysql.select("SELECT `id`, `passwo555rd` FROM `users` WHERE `email`=%s")

        finally:
            mysql=mysql_test()
            mysql.drop()








        