from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('pg', views.pg, name='pg'),
    path('mysql', views.mysql, name='mysql'),
]