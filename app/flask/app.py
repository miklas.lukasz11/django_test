from flask import Flask
import os
import sys
import requests

app = Flask(__name__)

# import logging
# log = logging.getLogger('werkzeug')
# log.setLevel(logging.ERROR)
# logging.disable(logging.CRITICAL)
@app.route('/')
def hello_world():
    os.environ['REVDEBUG_SOLUTION']="fds"
    return 'Hello, World!'

@app.route('/err')
def err():
    raise "aaa"
    return 'Hello, World!'

@app.route('/req')
def err2():
    raise RuntimeError('Not')
    return 'Hello, World!'


@app.route('/test')
def test():
    for x in range(1,1000):
        print(x)
    raise "You shall not pass!"
    return 'Hello, World!'

# import time

# r = requests.get('http://40.89.147.34:7000/err', auth=('user', 'pass'))
# sys.exit(0)